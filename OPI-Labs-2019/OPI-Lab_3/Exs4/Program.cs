﻿using System;
using System.Text;
using static System.Console;

namespace Exs4
{
    class Program
    {
        class EventGeneration
        {
            int eventAmount;
            public int EventAmount { set { eventAmount = value; } }

            bool index = false;
            public bool Index { set { index = value; } }

            public delegate void EventHandler(object sender, EventArgs e);
            public event EventHandler OnEventHandler;

            public void NewEvent()
            {
                EventArgs e = new EventArgs();
                if (OnEventHandler != null)
                {
                    WriteLine("Перша подія згенерована");
                    OnEventHandler(this, e);
                }
                if (index == true)
                {
                    WriteLine("Повідомлення отримано!");

                    for (int i = 0; i < eventAmount; i++)
                    {
                        WriteLine($"Подія {i + 2} згенерована");
                        OnEventHandler(this, e);
                    }
                }
            }
        }

        class Receiver
        {
            void OnHandler(object source, EventArgs e)
            {
                EventGeneration a = source as EventGeneration;
                a.Index = true;
            }

            public Receiver(EventGeneration genEvent)
            {
                genEvent.OnEventHandler += new EventGeneration.EventHandler(OnHandler);
            }
        }

        static void Main(string[] args)
        {
            Title = "Лабораторна робота №3. Завдання 4. Харипончук Марії, гр. ПІ-60";
            SetWindowSize(55, 15);
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;

            EventGeneration genEvent = new EventGeneration();
            Receiver recEvent = new Receiver(genEvent);

            int num;
            bool check;
            Write("Кількість повідомлень, які потрібно згенерувати: ");
            do
            {
                if ((check = int.TryParse(ReadLine(), out num)) != true)
                    WriteLine("!!!Помилка. Введіть коректне значення!");
            }
            while (!check);
            genEvent.EventAmount = num;
            genEvent.NewEvent();
            ReadKey(true);
        }
    }
}
