﻿using System;
using System.Text;
using static System.Console;

namespace Exs1
{
    class Program
    {
        delegate char Symbol(char symbol);
        delegate bool SymbolBool(char symbol);

        class SymbolProcessor
        {
            public static bool symbolIsNum(char symbol)
            {
                bool number;
                if (symbol >= 48 && symbol <= 57)
                    number = true;
                else
                    number = false;
                return number;
            }

            public bool symbolIsPunctMark(char symbol)
            {
                //! " ' ( ) , - . : ; ? „ … “ ” – — « »
                bool punctMark;
                if (symbol == 33 || symbol == 34 || symbol >= 39 && symbol <= 41 || symbol >= 44 && symbol <= 46 || symbol == 58 || symbol == 59 || symbol == 63 || symbol == 132 || symbol == 133 || symbol == 147 || symbol == 148 || symbol == 150 || symbol == 151 || symbol == 171 || symbol == 187)
                    punctMark = true;
                else
                    punctMark = false;
                return punctMark;
            }

            public char symbolNotLetter(char symbol)
            {
                if (symbol >= 65 && symbol <= 90 || symbol >= 97 && symbol <= 122 || symbol >= 1040 && symbol <= 1103)
                    return symbol;
                else
                {
                    symbol = (char)53;
                }
                return symbol;
            }
        }

        class ProcessText
        {
            private string text;
            private int cursorPos;
            private char? currentSymb;


            public ProcessText(string text)
            {
                this.text = text;
                cursorPos = -1;

                advance();
            }

            public void Reset()
            {
                cursorPos = -1;
                advance();
            }

            private void advance()
            {
                cursorPos++;

                if (cursorPos > text.Length - 1)
                {
                    currentSymb = null;
                    return;
                }
                currentSymb = text[cursorPos];
            }

            public string process(Symbol processor)
            {
                if (processor == null)
                    return text;

                string newText = "";

                while (currentSymb != null)
                {
                    newText += processor((char)currentSymb);

                    advance();
                }
                return newText;
            }

            public string processIs(SymbolBool processor)
            {
                if (processor == null)
                    return text;

                string newText = "";

                while (currentSymb != null)
                {
                    if (processor == SymbolProcessor.symbolIsNum)
                    {
                        bool isNum = SymbolProcessor.symbolIsNum((char)currentSymb);

                        if (isNum == true)
                            newText += '*';
                        else
                            newText += (char)currentSymb;
                    }
                    else
                    {
                        bool isPunctMark = new SymbolProcessor().symbolIsPunctMark((char)currentSymb);

                        if (isPunctMark == true)
                            newText += '*';
                        else
                            newText += (char)currentSymb;
                    }
                    advance();
                }
                return newText;
            }
        }

        static void Main(string[] args)
        {
            Title = "Лабораторна робота №3. Завдання 1. Харипончук Марії, гр. ПІ-60";
            SetWindowSize(40, 10);
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;

            ProcessText processText = new ProcessText("123,?Hello мир!");

            WriteLine("Строчка: \n" + "123,?Hello мир!\n");
            WriteLine(processText.processIs(SymbolProcessor.symbolIsNum));
            processText.Reset();
            WriteLine(processText.processIs(new SymbolProcessor().symbolIsPunctMark));
            processText.Reset();
            WriteLine(processText.process(new SymbolProcessor().symbolNotLetter));
            WriteLine();
        }
    }
}