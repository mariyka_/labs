﻿using System;
using System.Text;
using static System.Console;

namespace Exs2_3
{
    class Program
    {
        delegate bool PropertyChanged(string propertyName, string propertyValue);

        class CarService
        {
            string carName;
            public event PropertyChanged OnCarNameChanged = null;

            public CarService()
            {
                carName = "Ferrari";
            }

            public string GetCarName()
            {
                return carName;
            }

            public void ChangeCarName(string newName)
            {
                bool changeName = false;

                if (OnCarNameChanged != null)
                {
                    changeName = OnCarNameChanged.Invoke("OnCarNameChanged", newName);
                }

                if (changeName)
                {
                    carName = newName;
                    WriteLine("Назва авто змінилась!");
                    return;
                }
                WriteLine("Назва авто не була змінена!");
            }
        }

        static class CarOwner
        {
            public static bool OnCarNameChanged(string propertyName, string newValue)
            {
                WriteLine();
                WriteLine("Змінити назву автомобіля? \n\t Y / Yes / Так \n\t N / No / Ні");
                string input = InputHendler.getStringInput();
                input = input.ToLower();

                if (input == "yes" || input == "y" || input == "так")
                {
                    return true;
                }
                return false;
            }
        }

        static class InputHendler
        {
            public static void DisplayMenu()
            {
                WriteLine("\n\t* * * Меню: * * *");
                WriteLine("1. Перейменувати назву автомобіля" + "\n0. Вихід");
            }

            public static int getNumInput(int min, int max)
            {
                bool repeat = false;
                int num = 0;

                do
                {
                    string input = ReadLine();

                    try
                    {
                        num = int.Parse(input);
                    }
                    catch (Exception)
                    {
                        repeat = true;
                        WriteLine("!!!Помилка. Введіть коректне значення!");
                    }

                    if (num < min || num > max)
                    {
                        repeat = true;
                        WriteLine("!!!Помилка. Введіть коректне значення!");
                    }
                } while (repeat);
                return num;
            }

            public static string getStringInput()
            {
                string input = ReadLine();
                return input;
            }
        }
        static void Main(string[] args)
        {
            Title = "Лабораторна робота №3. Завдання 2-3. Харипончук Марії, гр. ПІ-60";
            SetWindowSize(40, 20);
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;

            CarService service = new CarService();
            service.OnCarNameChanged += CarOwner.OnCarNameChanged;

            do
            {
                WriteLine("Назва автомобіля: " + service.GetCarName());
                ReadKey();
                Clear();

                InputHendler.DisplayMenu();
                int input = InputHendler.getNumInput(0, 1);

                if (input == 1)
                {
                    WriteLine("Введіть нову назву автомобіля: ");
                    string newCarName = InputHendler.getStringInput();
                    service.ChangeCarName(newCarName);
                }
                else
                {
                    WriteLine("Ви вийшли з програми!\n");
                    return;
                }
            } while (true);
        }
    }
}
