﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class game_manager : MonoBehaviour
{
    public float ReloadDelay = 2F;

    public void EndGame()
    {
        Invoke("StartFromBegining", ReloadDelay);
    }

    public void ComplateLevel()
    {
        Invoke("LoadNextScene", ReloadDelay);
    }

    public void RestartGame()
    {
        Invoke("StartFromBegining", ReloadDelay);
    }

    private void LoadNextScene()
    {
        if (SceneManager.GetActiveScene().name == "FinalLevel")
            Application.Quit();
        else
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void StartFromBegining()
    {
        SceneManager.LoadScene(0);
    }
}
