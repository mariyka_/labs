﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class game_over : MonoBehaviour
{
    public GameObject GameOverUI;
    public Text GameOverScore;
    public GameObject player;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            collision.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            collision.gameObject.GetComponent<movement>().enabled = false;
            collision.gameObject.GetComponent<Animator>().enabled = false;

            GameOverScore.text = "Score: " + player.GetComponent<score>().GetScore();
            GameOverUI.SetActive(true);
        }
    }
}
