﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public float Speed = 10;
    public float JumpForce = 10;

    public Transform GroundChecker;
    public float checkerRadious;
    public LayerMask WhatIsGround;

    #region Private members

    private Rigidbody2D rb;
    private Animator anim;

    private float horizontalMove = 0;

    bool pointedRight = true;

    bool Jump = false;
    bool isGrounded = true;

    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        checkKeyboardEvents();
        moveCharacter(Time.deltaTime);
    }

    private void checkKeyboardEvents()
    {
        isGrounded = Physics2D.OverlapCircle(GroundChecker.position, checkerRadious, WhatIsGround);
        horizontalMove = UnityEngine.Input.GetAxis("Horizontal");

        if (!isGrounded)
            anim.SetBool("isJumping", true);
        else
            anim.SetBool("isJumping", false);

        if (horizontalMove == 0)
            anim.SetBool("isRunning", false);
        else
            anim.SetBool("isRunning", true);

        Jump = UnityEngine.Input.GetButtonDown("Jump");

        if (!pointedRight && horizontalMove > 0)
            Flip();
        else if (pointedRight && horizontalMove < 0)
            Flip();
    }

    private void moveCharacter(float deltaTime)
    {
        rb.velocity = new Vector2(horizontalMove * Speed, rb.velocity.y);

        if (Jump && isGrounded)
        {
            rb.velocity = new Vector2(0, JumpForce);
        }
    }

    private void Flip()
    {
        pointedRight = !pointedRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}