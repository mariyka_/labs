﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour
{
    int player_score = 0;
    public Text TextElement;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Pickable")
        {
            player_score++;
            TextElement.text = "Score: " + player_score;
            Destroy(collision.gameObject);
        }
    }

    public int GetScore()
    {
        return player_score;
    }
}
