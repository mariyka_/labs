﻿using System;
using System.Text;
using static System.Console;

namespace Matrix
{
    class Matrix
    {
        Random rand = new Random();
        private int Columns;
        private int Rows;
        private double[,] matrix;

        public Matrix(int rows, int columns)
        {
            this.Columns = columns;
            this.Rows = rows;

            matrix = new double[rows, columns];
        }

        public void FillMatrixRandomly(int seed)
        {
            rand = new Random(seed);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    matrix[i, j] = rand.Next(20);
                }
            }
        }

        public Matrix Multiplication(Matrix b)
        {
            Matrix newMatrix = new Matrix(Rows, b.Columns);

            if (Columns != b.Rows)
                throw new Exception("!!!Помилка. Матриці не можна перемножити");
            else
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < b.Columns; j++)
                    {
                        for (int k = 0; k < b.Rows; k++)
                        {
                            newMatrix.matrix[i, j] += matrix[i, k] * b.matrix[k, j];
                        }
                    }
                }
            }
            return newMatrix;
        }

        public double Determinant()
        {
            if (Columns != Rows)
                throw new Exception("!!!Помилка. Визначник може бути розрахований тільки для квадратної матриці");

            double det = 0;

            if (Columns == 1)
                det = matrix[0, 0];
            else if (Columns == 2)
                det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            else
            {
                for (int i = 0; i < Columns; i++)
                {
                    Matrix newSubMatrix = excludeRowAndColumn(row: 0, column: i);

                    det += Math.Pow(-1.0, i % 2.0) * matrix[0, i] * newSubMatrix.Determinant();
                }
            }
            return det;
        }

        private Matrix excludeRowAndColumn(int row, int column)
        {
            Matrix newMatrix = new Matrix(Rows - 1, Columns - 1);

            for (int i = 0, newI = 0; i < Rows; i++)
            {
                if (i == row)
                    continue;

                for (int j = 0, newJ = 0; j < Columns; j++)
                {
                    if (j == column)
                        continue;

                    newMatrix.matrix[newI, newJ] = matrix[i, j];
                    newJ++;
                }
                newI++;
            }
            return newMatrix;
        }

        public Matrix TransposedMatrix()
        {
            Matrix newMatrix = new Matrix(Columns, Rows);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    newMatrix.matrix[j, i] = matrix[i, j];
                }
            }
            return newMatrix;
        }

        public Matrix InversedMatrix()
        {
            double determinant = Determinant();
            if (determinant == 0)
                return null;

            Matrix newMatrix = new Matrix(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    double value = Math.Pow(-1.0, (i + j) % 2.0) * (excludeRowAndColumn(i, j)).Determinant();

                    newMatrix.matrix[j, i] = value;
                }
            }

            return newMatrix * (1 / determinant);
        }

        public Matrix Multiplication(double number)
        {
            Matrix newMatrix = new Matrix(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    newMatrix.matrix[i, j] = matrix[i, j] * number;
                }
            }
            return newMatrix;
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            return a.Multiplication(b);
        }

        public static Matrix operator *(Matrix oneMatrix, double number)
        {
            return oneMatrix.Multiplication(number);
        }

        public void PrintMatrix()
        {
            WriteLine(" Матриця:\n");

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Write($"{Math.Round(matrix[i, j], 3),6}\t");
                }
                WriteLine();
            }
        }
    }

    class Program
    {
        static void PrintMenu()
        {
            Write("\n- - - - - - Меню - - - - - -\n" +
                "1. Множення матриць\n" +
                "2. Знаходження визначника\n" +
                "3. Знаходження транспонованої матриці\n" +
                "4. Знаходження оберненої матриці\n" +
                "0. Вихід\n-> ");
        }

        static void Main(string[] args)
        {
            Title = "Лабораторна робота №1. Харипончук Марії, гр. ПІ-60";
            SetWindowSize(60, 20);
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;

            int menu;
            bool check;

            do
            {
                Clear();
                PrintMenu();
                do
                {
                    if ((check = int.TryParse(ReadLine(), out menu)) != true)
                        WriteLine("!!!Помилка. Введіть коректне значення!");
                }
                while (!check);

                switch (menu)
                {
                    case 1:
                        {
                            Clear();
                            Write("\t*** Множення матриць ***\n\n");
                            Matrix a = new Matrix(3, 4);
                            a.FillMatrixRandomly(1);
                            a.PrintMatrix();
                            Matrix b = new Matrix(4, 2);
                            b.FillMatrixRandomly(2);
                            b.PrintMatrix();
                            (a * b).PrintMatrix();
                            ReadKey();
                            break;
                        }
                    case 2:
                        {
                            Clear();
                            Write("\t*** Знаходження визначника ***\n\n");
                            Matrix a = new Matrix(3, 3);
                            a.FillMatrixRandomly(1);
                            a.PrintMatrix();
                            WriteLine($"\n Визначник: {a.Determinant()}");
                            ReadKey();
                            break;
                        }
                    case 3:
                        {
                            Clear();
                            Write("\t*** Знаходження транспонованої матриці ***\n\n");
                            Matrix a = new Matrix(3, 4);
                            a.FillMatrixRandomly(1);
                            a.PrintMatrix();
                            a.TransposedMatrix().PrintMatrix();
                            ReadKey();
                            break;
                        }
                    case 4:
                        {
                            Clear();
                            Write("\t*** Знаходження оберненої матриці ***\n\n");
                            Matrix a = new Matrix(3, 3);
                            a.FillMatrixRandomly(1);
                            a.PrintMatrix();
                            a.InversedMatrix().PrintMatrix();
                            ReadKey();
                            break;
                        }
                    case 0:
                        WriteLine("Ви вийшли з програми!\n");
                        break;
                    default:
                        WriteLine("!!!Помилка. Введіть коректне значення!");
                        ReadKey();
                        break;
                }
            } while (menu != 0);
        }
    }
}
