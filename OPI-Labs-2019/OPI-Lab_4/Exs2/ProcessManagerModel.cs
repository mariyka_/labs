﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Exs2
{
    class ProcessData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string Priority { get; set; }
        public string ThreadsCount { get; set; }
        public string Ram { get; set; }

        public ProcessData(Process process)
        {
            Id = process.Id.ToString();
            Name = process.ProcessName;
            StartTime = process.StartTime.ToShortTimeString();
            Priority = process.PriorityClass.ToString();
            ThreadsCount = process.Threads.Count.ToString();
            Ram = process.PagedMemorySize64.ToString();
        }
    }

    class ProcessManagerModel : BaseModel
    {

        #region Public Members

        public Command OpenProcess { get; set; }
        public Command UpdateProcesses { get; set; }
        public Command KillProcess { get; set; }

        public IEnumerable<ProcessPriorityClass> ComboboxItems { get; set; }
        public ProcessPriorityClass ComboboxSelectedItem
        {
            get => comboboxSelectedItem;

            set
            {
                comboboxSelectedItem = value;
                OnPropertyChanged(nameof(ComboboxSelectedItem));
            }
        }

        public bool IsKillProcessActive
        {
            get => isKillProcessActive;

            set
            {
                isKillProcessActive = value;
                OnPropertyChanged(nameof(IsKillProcessActive));
            }
        }
        public bool IsPriorityChooseActive
        {
            get => isPriorityChooseActive;

            set
            {
                isPriorityChooseActive = value;
                OnPropertyChanged(nameof(IsPriorityChooseActive));
            }
        }

        public List<ProcessData> Processes
        {
            get => processes;

            set
            {
                processes = value;
                OnPropertyChanged(nameof(Processes));
            }
        }

        public ProcessData SelectedItem
        {
            get => selectedItem;

            set
            {
                selectedItem = value;
                OnPropertyChanged(nameof(SelectedItem));
            }
        }

        #endregion

        #region Private members

        ProcessPriorityClass comboboxSelectedItem;

        bool isKillProcessActive;
        bool isPriorityChooseActive;

        ProcessData selectedItem;
        List<ProcessData> processes;

        #endregion

        public ProcessManagerModel()
        {
            IsKillProcessActive = false;
            IsPriorityChooseActive = false;

            ComboboxSelectedItem = ProcessPriorityClass.Normal;
            ComboboxItems = Enum.GetValues(typeof(ProcessPriorityClass)).Cast<ProcessPriorityClass>();

            PropertyChanged += onItemChoosed;
            PropertyChanged += onComboboxItemChoosed;

            loadProcesses();

            OpenProcess = new Command((obj) => {
                string process = obj as string;

                openProcess(process);
            });

            UpdateProcesses = new Command((obj) =>
            {
                loadProcesses();
            });

            KillProcess = new Command((obj) =>
            {
                if (SelectedItem != null)
                {
                    int processId = int.Parse(SelectedItem.Id);

                    killProcess(processId);
                }
            });
        }

        #region Private methods

        void onItemChoosed(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ComboboxSelectedItem))
            {
                int processId = int.Parse(SelectedItem.Id);

                changePriority(processId, ComboboxSelectedItem);
            }
        }

        void onComboboxItemChoosed(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedItem))
            {
                if (SelectedItem != null)
                {
                    IsKillProcessActive = true;
                    IsPriorityChooseActive = true;
                }
                else
                {
                    IsKillProcessActive = false;
                    IsPriorityChooseActive = false;
                }
            }
        }

        void loadProcesses()
        {
            List<ProcessData> temp = new List<ProcessData>();

            foreach (var process in Process.GetProcesses())
            {
                try
                {
                    var _ = process.StartTime;
                    var data = new ProcessData(process);
                    temp.Add(data);
                }
                catch (Exception)
                {
                    Console.WriteLine("-", process.ProcessName);
                }
            }
            Processes = temp;
        }

        void openProcess(string process)
        {
            try
            {
                Process.Start(process);
            }
            catch (Exception)
            {
            }
        }

        void killProcess(int processId)
        {
            try
            {
                Process.GetProcessById(processId).Kill();
            }
            catch (Exception)
            {
            }

            loadProcesses();
        }

        void changePriority(int processId, ProcessPriorityClass priority)
        {
            try
            {
                Process.GetProcessById(processId).PriorityClass = priority;
            }
            catch (Exception)
            {
            }

            loadProcesses();
        }
        #endregion
    }
}
