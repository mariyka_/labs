﻿using System.Windows;
using System.ComponentModel;

namespace Exs1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new EncrypterModel();
        }
    }
}
