﻿using System;
using System.IO;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.Diagnostics;
using System.ComponentModel;

namespace Exs1
{
    class EncrypterModel : BaseModel
    {
        #region public properties

        public bool IsEncryptActive
        {
            get => isEncryptActive;
            set
            {
                isEncryptActive = value;

                OnPropertyChanged("IsEncryptActive");
            }
        }

        public bool IsDecryptActive
        {
            get => isDecryptActive;
            set
            {
                isDecryptActive = value;

                OnPropertyChanged("IsDecryptActive");
            }
        }

        public bool IsChooseFileActive
        {
            get => isChooseFileActive;
            set
            {
                isChooseFileActive = value;

                OnPropertyChanged("IsChooseFileActive");
            }
        }

        public byte Progress
        {
            get => progress;

            set
            {
                if (value > 100)
                    value = 100;
                else if (value < 0)
                    value = 0;

                progress = value;

                OnPropertyChanged("Progress");
            }
        }

        public string Logs
        {
            get => logs;

            set
            {
                logs = value;

                OnPropertyChanged("Logs");
            }
        }

        public Command EncryptCommand { get; set; }
        public Command DecryptCommand { get; set; }
        public Command ChooseFile { get; set; }

        #endregion

        #region private properties

        private string choosedFile;

        private byte[] salt;
        private byte progress = 0;
        private string logs = "";

        public bool isEncryptActive = false;
        public bool isDecryptActive = false;
        public bool isChooseFileActive = true;

        private int bufferSize = 1024;

        private string fileName;
        private long fileSize;

        private BackgroundWorker bgWorker;

        #endregion

        #region Constructor

        public EncrypterModel()
        {
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.ProgressChanged += progressChangedHandler;
            bgWorker.DoWork += onGBWorkerRun;
            bgWorker.RunWorkerCompleted += onBGWorkerComplate;

            EncryptCommand = new Command((obj) =>
            {
                // Pass true as the argument to indicate that it's the encrypting command
                bgWorker.RunWorkerAsync(argument: new object[] { true, obj });
            });

            DecryptCommand = new Command((obj) =>
            {
                // Pass false as the argument to indicate that it's the decrypting command
                bgWorker.RunWorkerAsync(argument: new object[] { false, obj });
            });

            ChooseFile = new Command((obj) =>
            {
                chooseFile();

                Log($"Выбранный файл: {choosedFile}", MessageType.Info, LoggingType.New);

                IsDecryptActive = true;
                IsEncryptActive = true;
            });
        }
        #endregion

        #region Private methods

        private void onGBWorkerRun(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            IsChooseFileActive = false;
            IsDecryptActive = false;
            IsEncryptActive = false;

            bool isEncrypt = (bool)((object[])e.Argument)[0];

            string log = "";
            string error = "";

            try
            {
                if (isEncrypt)
                {
                    log = encryptCommand(((object[])e.Argument)[1], worker);
                }
                else
                {
                    log = decryptCommand(((object[])e.Argument)[1], worker);
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                IsChooseFileActive = true;
                IsDecryptActive = true;
                IsEncryptActive = true;

                e.Result = new object[] { log, error };
            }
        }

        private void onBGWorkerComplate(object sender, RunWorkerCompletedEventArgs e)
        {
            string error = (string)((object[])e.Result)[1];
            string log = (string)((object[])e.Result)[0];

            if (e.Error != null)
            {
                Log(e.Error.ToString(), MessageType.Error);
            }
            else if (error != "")
            {
                Log(error, MessageType.Error);
            }
            else
            {
                Log(log.ToString(), MessageType.Success);
            }
        }

        private string encryptCommand(object obj, BackgroundWorker worker)
        {
            string password = (string)obj;

            if (password.Length < 4)
                throw new Exception("Пароль должен содержать не менее 4 символов");

            worker.ReportProgress(0);

            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                encrypt(choosedFile, password, worker);

                sw.Stop();
                var elapsedTime = sw.Elapsed.TotalSeconds;

                return $"Имя файла: {fileName}\nРазмер файла: {fileSize}b\nИстекшие секунды: {elapsedTime}";
            }
            catch (Exception e)
            {
                throw new Exception("Ошибка во время шифрования:\n\n" + e);
            }
        }

        private string decryptCommand(object obj, BackgroundWorker worker)
        {
            string password = (string)obj;

            worker.ReportProgress(0);

            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                decrypt(choosedFile, password, worker);

                sw.Stop();
                var elapsedTime = sw.Elapsed.TotalSeconds;

                return $"Имя файла: {fileName}\nРазмер файла: {fileSize}b\nИстекшие секунды: {elapsedTime}";
            }
            catch (Exception e)
            {
                throw new Exception("Ошибка во время шифрования:\n\n" + e);
            }
        }

        private void Log(string message, MessageType messageType, LoggingType loggingType = LoggingType.Append)
        {
            if (loggingType == LoggingType.New)
                Logs = "";

            switch (messageType)
            {
                case MessageType.Error:
                    message = "\n\n * * * * * Ошибка * * * * * \n\n" + message;
                    break;
                case MessageType.Info:
                    message = "\n\n * * * * * Информация * * * * * \n\n" + message;
                    break;
                case MessageType.Success:
                    message = "\n\n * * * * * Успешно * * * * *\n\n" + message;
                    break;
                case MessageType.Warning:
                    message = "\n\n * * * * * Предупреждение * * * * *\n\n" + message;
                    break;
                default:
                    break;
            }
            Logs += message;
        }

        private enum MessageType
        {
            Error,
            Info,
            Success,
            Message,
            Warning
        }

        private enum LoggingType
        {
            Append,
            New
        }

        private void progressChangedHandler(object sender, ProgressChangedEventArgs e)
        {
            var progress = e.ProgressPercentage;

            Progress = (byte)progress;
        }

        private void chooseFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "All Files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                choosedFile = openFileDialog.FileName;
            }
        }

        private void encrypt(string fileToEncrypt, string password, BackgroundWorker worker)
        {
            salt = generateSalt();

            fileName = fileToEncrypt + ".enc";
            FileStream encryptedFile = new FileStream(fileName, FileMode.Create);

            byte[] bytePass = System.Text.Encoding.UTF8.GetBytes(password);

            // Encrypting algorythm
            RijndaelManaged algoEnc = new RijndaelManaged();
            algoEnc.KeySize = 256;
            algoEnc.BlockSize = 128;
            algoEnc.Padding = PaddingMode.PKCS7;

            var key = new Rfc2898DeriveBytes(bytePass, salt, 50000);
            algoEnc.Key = key.GetBytes(algoEnc.KeySize / 8);
            algoEnc.IV = key.GetBytes(algoEnc.BlockSize / 8);

            algoEnc.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(encryptedFile, algoEnc.CreateEncryptor(), CryptoStreamMode.Write);
            FileStream fileToEncryptStream = new FileStream(fileToEncrypt, FileMode.Open);

            byte[] buffer = new byte[bufferSize];
            int read;

            int progress = 10;
            worker.ReportProgress(progress);

            fileSize = fileToEncryptStream.Length;
            var bytesPerFivePersent = fileSize / 18;
            var tempSize = bytesPerFivePersent;

            try
            {
                while ((read = fileToEncryptStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    tempSize -= read;
                    if (tempSize <= 0)
                    {
                        progress += 5;
                        worker.ReportProgress(progress);
                        tempSize = bytesPerFivePersent + tempSize;
                    }

                    cs.Write(buffer, 0, read);
                }
                worker.ReportProgress(100);
            }
            finally
            {
                cs.Close();
                fileToEncryptStream.Close();
                encryptedFile.Close();
            }
        }

        private void decrypt(string fileToDecrypt, string password, BackgroundWorker worker)
        {
            if (Path.GetExtension(fileToDecrypt) != ".enc")
                throw new FileFormatException("Только зашифрованный файл (с расширением .enc) может быть расшифрован");

            byte[] bytePass = System.Text.Encoding.UTF8.GetBytes(password);

            FileStream encrFile = new FileStream(fileToDecrypt, FileMode.Open);

            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(bytePass, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            string parentDir = Path.GetDirectoryName(fileToDecrypt);
            string fileName = Path.GetFileNameWithoutExtension(fileToDecrypt);

            fileName = parentDir + "\\" + "decrypted-" + fileName;
            FileStream decryptedFile = new FileStream(fileName, FileMode.Create);
            CryptoStream decryptor = new CryptoStream(encrFile, AES.CreateDecryptor(), CryptoStreamMode.Read);

            int read;
            byte[] buffer = new byte[bufferSize];

            int progress = 10;
            worker.ReportProgress(progress);

            fileSize = encrFile.Length;
            var bytesPerFivePersent = fileSize / 18;
            var tempSize = bytesPerFivePersent;

            try
            {
                while ((read = decryptor.Read(buffer, 0, buffer.Length)) > 0)
                {
                    tempSize -= read;
                    if (tempSize <= 0)
                    {
                        progress += 5;
                        worker.ReportProgress(progress);
                        tempSize = bytesPerFivePersent + tempSize;
                    }
                    decryptedFile.Write(buffer, 0, read);
                }
                worker.ReportProgress(100);
            }
            finally
            {
                try
                {
                    decryptor.Close();
                }
                finally
                {
                    encrFile.Close();
                    decryptedFile.Close();
                }
            }

        }

        byte[] generateSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    rng.GetBytes(data);
                }
            }
            return data;
        }
        #endregion
    }
}
