﻿using System;
using System.Windows.Input;

namespace Exs1
{
    class Command : ICommand
    {
        Action<object> action;

        public Command(Action<object> action)
        {
            this.action = action;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            // Always can be executed
            return true;
        }

        public void Execute(object parameter)
        {
            action?.Invoke(parameter);
        }
    }
}
