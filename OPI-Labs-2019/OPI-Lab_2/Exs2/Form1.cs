﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exs2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public delegate double SolverDelegate(double a, double b);

        private async void buttonStart_Click(object sender, EventArgs e)
        {
            labelInfo.Text = "Йде розрахунок ...";
            buttonStart.Enabled = false;
            
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var res = await processAsync();
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Console.WriteLine(res);
            labelTime.Text = "Витрачено часу: " + (elapsedMs).ToString() + " мс";
            textBoxResult.Text = res.ToString();
            labelInfo.Text = "Розрахунок завершено!";
            buttonStart.Enabled = true;
        }

        private async Task<double> processAsync()
        {
            var result = await Task.Run(() =>
            {
                double res = 0;
                
                for (float i = -20000; i < 20000; i += 0.01F)
                {
                    double firstPart = Math.Abs(Math.Pow(i, 2) - 5);
                    double secondPart = i - 5;
                    res += firstPart / secondPart;
                }
                return Math.Round(res, 2);
            });
            return result;
        }
    }
}
