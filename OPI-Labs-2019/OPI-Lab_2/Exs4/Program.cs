﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using static System.Console;

namespace Exs4
{
    class Program
    {
        class Book
        {
            public readonly string Name;
            public readonly string Author;
            public readonly string Publisher;

            public Book(string name, string author, string publisher)
            {
                Name = name;
                Author = author;
                Publisher = publisher;
            }
        }

        class Library
        {
            private List<Book> list = new List<Book>();

            public void AddBook(Book book)
            {
                list.Add(book);
            }

            public void RemoveBook(Book book)
            {
                list.Remove(book);
            }

            public List<Book> GetBooks()
            {
                return new List<Book>(list);
            }

            public void PrintBooks()
            {
                foreach (var book in list)
                {
                    WriteLine(string.Format($"{book.Name, -20}") + " - " + string.Format($"{book.Author, -23}") + " - " + string.Format($"{book.Publisher, -8}"));
                }
                WriteLine();
            }

            public void Sort(Comparison<Book> comparer)
            {
                list.Sort(comparer);
            }
        }

        class BooksSorterBuilder
        {
            public Comparison<Book> SortBookBy(string fieldName)
            {
                string functionName = "sortBy" + fieldName;

                Type thisType = typeof(BooksSorterBuilder);

                MethodInfo method = null;
                try
                {
                    method = thisType.GetMethod(functionName, BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(Book), typeof(Book) }, null);
                }
                catch (Exception e)
                {
                    WriteLine(e.Message);
                    throw new Exception("Такого сортування функції '" + functionName + "' не існує");
                }

                Comparison<Book> comparMethod = (Comparison<Book>)Delegate.CreateDelegate(typeof(Comparison<Book>), this, method);
                return comparMethod;
            }

            private int sortByName(Book first, Book second)
            {
                return sortAlphabet(first.Name, second.Name);
            }

            private int sortByAuthor(Book first, Book second)
            {
                return sortAlphabet(first.Author, second.Author);
            }

            private int sortByPublisher(Book first, Book second)
            {
                return sortAlphabet(first.Publisher, second.Publisher);
            }

            private int sortAlphabet(string first, string second)
            {
                int smallestStr = Math.Min(first.Length, second.Length);

                for (int i = 0; i < smallestStr; i++)
                {
                    char first_sym = first[i];
                    char second_sym = second[i];

                    if (first_sym >= 65 && first_sym <= 90)
                        first_sym = (char)(first_sym - 65);

                    if (second_sym >= 65 && second_sym <= 90)
                        second_sym = (char)(second_sym - 65);

                    if (second_sym >= 97 && second_sym <= 122)
                        second_sym = (char)(second_sym - 97);

                    if (first_sym >= 97 && first_sym <= 122)
                        first_sym = (char)(first_sym - 97);

                    if (first_sym > second_sym)
                        return 1;
                    else if (first_sym < second_sym)
                        return -1;
                    else if (first_sym < second_sym)
                        return 0;
                }

                if (first.Length > second.Length)
                    return 1;
                return -1;
            }
        }

        static void Main(string[] args)
        {
            Title = "Лабораторна робота №2. Завдання 4. Харипончук Марії, гр. ПІ-60";
            SetWindowSize(65, 35);
            OutputEncoding = Encoding.Unicode;
            InputEncoding = Encoding.Unicode;
            
            Book first = new Book("Маленький принц", "Антуан де Сент-Экзюпери", "Галлимар");
            Book second = new Book("Портрет Дориана Грея", "Оскар Уайльд", "Азбука");
            Book third = new Book("Пока Я не Я", "Дмитрий Троцкий", "АСТ");
            Book fourth = new Book("Самурай без меча", "Китами Масао", "Попурри");
            Book fifth = new Book("Шерлок Холмс", "Артур Конан Дойл", "Амфора");
            
            Library lib = new Library();
            lib.AddBook(first);
            lib.AddBook(second);
            lib.AddBook(third);
            lib.AddBook(fourth);
            lib.AddBook(fifth);

            BooksSorterBuilder builder = new BooksSorterBuilder();
            WriteLine("\t\t* * * Без сортування: * * *\n");
            lib.PrintBooks();
            
            lib.Sort(builder.SortBookBy("Name"));
            WriteLine("\t\t* * * Сортування по назві: * * *\n");
            lib.PrintBooks();

            lib.Sort(builder.SortBookBy("Author"));
            WriteLine("\t\t* * * Сортування по автору: * * *\n");
            lib.PrintBooks();

            lib.Sort(builder.SortBookBy("Publisher"));
            WriteLine("\t\t* * * Сортування по видавцтву: * * *\n");
            lib.PrintBooks();
        }
    }
}
