﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ServerLib
{
    public class MessageManager: IMesenger
    {

        public void SendMessage(string message)
        {
            Logger.Log("New Message: " + message, MessageType.Info);
        }

    }

    public class ProcessManager : IProcessManager
    {
        public List<ProcessData> GetProcesses()
        {
            List<ProcessData> temp = new List<ProcessData>();

            foreach (var process in Process.GetProcesses())
            {
                try
                {
                    var _ = process.StartTime;
                    var data = new ProcessData(process);
                    temp.Add(data);
                }
                catch (Exception)
                {
                }
            }

            return temp;
        }

        public void KillProcess(int processId)
        {
            try
            {
                Process.GetProcessById(processId).Kill();
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }


        public void ChangePriority(int processId, ProcessPriorityClass priority)
        {
            try
            {
                Process.GetProcessById(processId).PriorityClass = priority;
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }

        public void RunProcess(string process)
        {
            try
            {
                Process.Start(process);
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }
    }

    public class RemoteControlService : IRemoteControlService
    {
        public List<ProcessData> GetProcesses()
        {
            List<ProcessData> temp = new List<ProcessData>();

            foreach (var process in Process.GetProcesses())
            {
                try
                {
                    var _ = process.StartTime;
                    var data = new ProcessData(process);
                    temp.Add(data);
                }
                catch (Exception)
                {
                }
            }

            return temp;
        }

        public void KillProcess(int processId)
        {
            try
            {
                Process.GetProcessById(processId).Kill();
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }


        public void ChangePriority(int processId, ProcessPriorityClass priority)
        {
            try
            {
                Process.GetProcessById(processId).PriorityClass = priority;
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }

        public byte[] MakeScreenshot()
        {
            Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);
            }

            ImageConverter converter = new ImageConverter();

            return (byte[])converter.ConvertTo(bmp, typeof(byte[]));
        }

        public void RunProcess(string process)
        {
            try
            {
                Process.Start(process);
            }
            catch (Exception e)
            {
                Logger.Log(e.Message, MessageType.Error);
            }
        }

        public void SendMessage(string message)
        {
            Logger.Log("New Message: " + message, MessageType.Info);
        }

        public void ShutdownComputer()
        {
            Process.Start("shutdown", "/s /t 0");
        }
    }
}
