﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLib
{
    public enum MessageType
    {
        Info,
        Success,
        Warning,
        Error
    }

    public static class Logger
    {

        public static void Log(string message, MessageType messageType)
        {
            string additionalInfo = $"[{DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"):19}] ";

            switch (messageType)
            {
                case MessageType.Info:
                    additionalInfo += " [INFO] ";
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case MessageType.Success:
                    additionalInfo += " [SUCCESS] ";
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case MessageType.Warning:
                    additionalInfo += " [WARNING] ";
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case MessageType.Error:
                    additionalInfo += " [ERROR] ";
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }

            Console.WriteLine(additionalInfo + message);
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
