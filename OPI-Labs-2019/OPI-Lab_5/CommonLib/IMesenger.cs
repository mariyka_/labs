﻿using System.ServiceModel;

namespace CommonLib
{
    [ServiceContract]
    public interface IMesenger
    {
        [OperationContract]
        void SendMessage(string message);
    }
}
