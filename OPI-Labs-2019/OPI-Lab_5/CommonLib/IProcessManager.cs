﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace CommonLib
{
    [DataContract]
    public class ProcessData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string StartTime { get; set; }
        [DataMember]
        public string Priority { get; set; }
        [DataMember]
        public string ThreadsCount { get; set; }
        [DataMember]
        public string Ram { get; set; }

        public ProcessData(Process process)
        {
            Id = process.Id.ToString();
            Name = process.ProcessName;
            StartTime = process.StartTime.ToShortTimeString();
            Priority = process.PriorityClass.ToString();
            ThreadsCount = process.Threads.Count.ToString();
            Ram = process.PagedMemorySize64.ToString();
        }

    }

    [ServiceContract]
    public interface IProcessManager
    {
        [OperationContract]
        List<ProcessData> GetProcesses();
        [OperationContract]
        void KillProcess(int processId);
        [OperationContract]
        void RunProcess(string process);
        [OperationContract]
        void ChangePriority(int processId, ProcessPriorityClass priority);
    }
}
