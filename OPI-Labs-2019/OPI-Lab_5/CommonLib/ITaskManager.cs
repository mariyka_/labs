﻿using System.Drawing;
using System.ServiceModel;

namespace CommonLib
{
    [ServiceContract]
    public interface ITaskManager
    {
        [OperationContract]
        byte[] MakeScreenshot();
        [OperationContract]
        void ShutdownComputer();
    }
}
