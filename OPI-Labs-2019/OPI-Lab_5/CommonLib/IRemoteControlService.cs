﻿using System.ServiceModel;

namespace CommonLib
{
    [ServiceContract]
    public interface IRemoteControlService: IMesenger, ITaskManager, IProcessManager
    {

    }
}
