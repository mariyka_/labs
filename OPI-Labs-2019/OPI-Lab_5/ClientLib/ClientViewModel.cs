﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using ClientLib.RemoteControlService;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ClientLib
{
    public class ClientViewModel : BaseViewModel
    {

        #region Public Members

        #region Commands

        public Command OpenProcess { get; set; }
        public Command RunCustomProcess { get; set; }
        public Command UpdateProcesses { get; set; }
        public Command KillProcess { get; set; }

        public Command ConnectToServer { get; set; }
        public Command ShutdownServer { get; set; }

        public Command MakeScreenshot { get; set; }

        public Command SendMessage { get; set; }

        #endregion

        #region Flags

        public bool IsKillProcessActive
        {
            get => isKillProcessActive;

            set
            {
                isKillProcessActive = value;

                OnPropertyChanged(nameof(IsKillProcessActive));
            }
        }
        public bool IsPriorityChooseActive
        {
            get => isPriorityChooseActive;

            set
            {
                isPriorityChooseActive = value;

                OnPropertyChanged(nameof(IsPriorityChooseActive));
            }
        }
        public bool IsServerConnected
        {
            get => isServerConnected;

            set
            {
                isServerConnected = value;

                OnPropertyChanged(nameof(IsServerConnected));
            }
        }

        #endregion
        public IEnumerable<ProcessPriorityClass> ComboboxItems { get; set; }
        public ProcessPriorityClass ComboboxSelectedItem
        {
            get => comboboxSelectedItem;

            set
            {
                comboboxSelectedItem = value;

                OnPropertyChanged(nameof(ComboboxSelectedItem));
            }
        }

        public List<ProcessData> Processes
        {
            get => processes;

            set
            {
                processes = value;

                OnPropertyChanged(nameof(Processes));
            }
        }

        public ProcessData SelectedItem
        {
            get => selectedItem;

            set
            {
                selectedItem = value;

                OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public string CustomProcessName
        {
            get => customProcessName;

            set
            {
                customProcessName = value;

                OnPropertyChanged(nameof(CustomProcessName));
            }
        }

        public string ServerAddress
        {
            get => serverAddress;

            set
            {
                serverAddress = value;

                OnPropertyChanged(nameof(ServerAddress));
            }
        }

        public string ConnectionStatus
        {
            get => connectionStatus;

            set
            {
                connectionStatus = "Status: " + value;

                OnPropertyChanged(nameof(ConnectionStatus));
            }
        }

        RemoteControlService.IProcessManager processManager;
        RemoteControlService.IMesenger messageManager;
        RemoteControlService.ITaskManager taskManager;

        #endregion

        #region Private members

        ProcessPriorityClass comboboxSelectedItem;

        bool isKillProcessActive;
        bool isPriorityChooseActive;
        bool isServerConnected;

        ProcessData selectedItem;
        List<ProcessData> processes;

        string customProcessName;
        string serverAddress;
        string connectionStatus;

        string defauldServerIP = "127.0.0.1";
        Regex ipValidator = new Regex("^\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}$");

        #endregion

        #region Constructor


        public ClientViewModel()
        {
            prepareComponents();
            setupCommands();
        }

        void prepareComponents()
        {
            ConnectionStatus = "Disconnected";

            IsKillProcessActive = false;
            IsPriorityChooseActive = false;
            IsServerConnected = false;

            ComboboxSelectedItem = ProcessPriorityClass.Normal;
            ComboboxItems = Enum.GetValues(typeof(ProcessPriorityClass)).Cast<ProcessPriorityClass>();

            PropertyChanged += onItemChoosed;
            PropertyChanged += onComboboxItemChoosed;

            connectToService(defauldServerIP);
        }

        void setupCommands()
        {
            OpenProcess = new Command((obj) =>
            {
                string process = obj as string;

                runProcess(process);
            });

            UpdateProcesses = new Command((obj) =>
            {
                loadProcesses();
            });

            KillProcess = new Command((obj) =>
            {
                if (SelectedItem != null)
                {
                    int processId = int.Parse(SelectedItem.Id);

                    killProcess(processId);
                }
            });

            RunCustomProcess = new Command((obj) =>
            {
                string processName = (string)obj;
                runProcess(processName);
            });

            ConnectToServer = new Command((obj) =>
            {
                string address = (string)obj;
                if (String.IsNullOrWhiteSpace(address))
                    address = defauldServerIP;

                if (!validateServerAdress(address))
                {
                    MessageBox.Show("Invalid ip address, folow the pattern (127.0.0.1)", "Invalid ip address value");
                    return;
                }

                connectToService(address);
            });

            SendMessage = new Command((obj) =>
            {
                string message = (string)obj;

                sendMessage(message);


            });

            ShutdownServer = new Command((obj) =>
            {
                shutdownServer();
            });

            MakeScreenshot = new Command((obj) =>
            {
                makeScreenshot();
            });
        }

        #endregion

        #region Event Handlers

        void onItemChoosed(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ComboboxSelectedItem))
            {
                int processId = int.Parse(SelectedItem.Id);

                changePriority(processId, ComboboxSelectedItem);
            }
        }

        void onComboboxItemChoosed(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedItem))
            {
                if (SelectedItem != null)
                {
                    IsKillProcessActive = IsServerConnected;
                    IsPriorityChooseActive = IsServerConnected;
                }
                else
                {
                    IsKillProcessActive = false;
                    IsPriorityChooseActive = false;
                }
            }
        }

        #endregion

        #region Private methods

        bool validateServerAdress(string adress)
        {
            return ipValidator.IsMatch(adress);
        }

        void connectToService(string adress)
        {
            string baseAdress = "http://" + adress + ":8000/RemoteControlService";

            BasicHttpBinding binding = new BasicHttpBinding();

            try
            {
                var channelFactoryProcessManager = new ChannelFactory<RemoteControlService.IProcessManager>(binding, baseAdress);
                processManager = channelFactoryProcessManager.CreateChannel();

                var channelFactoryTaskManager = new ChannelFactory<RemoteControlService.ITaskManager>(binding, baseAdress);
                taskManager = channelFactoryTaskManager.CreateChannel();

                var channelFactoryMessageManager = new ChannelFactory<RemoteControlService.IMesenger>(binding, baseAdress);
                messageManager = channelFactoryMessageManager.CreateChannel();

                loadProcesses();
                IsServerConnected = true;
                ConnectionStatus = "Connected to " + adress;
            }
            catch (Exception e)
            {
                ConnectionStatus = "Error occurred during connection";
                MessageBox.Show(e.Message, "Error occurred during connection to " + adress, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void loadProcesses()
        {
            Processes = new List<ProcessData>(processManager.GetProcesses());
        }

        void runProcess(string process)
        {
            processManager.RunProcess(process);
        }

        void killProcess(int processId)
        {
            processManager.KillProcess(processId);

            loadProcesses();
        }

        void changePriority(int processId, ProcessPriorityClass priority)
        {
            processManager.ChangePriority(processId, priority);

            loadProcesses();
        }

        void shutdownServer()
        {
            taskManager.ShutdownComputer();

            IsServerConnected = false;
            IsKillProcessActive = false;
            IsPriorityChooseActive = false;

            ConnectionStatus = "Disconnected";
        }

        void sendMessage(string message)
        {
            messageManager.SendMessage(message);
        }

        void makeScreenshot()
        {

            byte[] screenshotData = taskManager.MakeScreenshot();
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Select folder to save screenshot";
                var result = dialog.ShowDialog();

                if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(dialog.SelectedPath))
                    using (MemoryStream stream = new MemoryStream(screenshotData))
                    {
                        Image img = Image.FromStream(stream);
                        FileStream fileStream = new FileStream(dialog.SelectedPath, FileMode.Create);
                        img.Save(fileStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
            }
        }
        #endregion

    }
}
