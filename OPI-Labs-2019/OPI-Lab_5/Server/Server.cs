﻿using CommonLib;
using ServerLib;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Server
{
    class Server
    {
        static void Main(string[] args)
        {
            Type contract = typeof(IRemoteControlService);
            Type messengerContract = typeof(IMesenger);
            Type processManagerContract = typeof(IProcessManager);
            Type taskManagerContract = typeof(ITaskManager);

            var binding = new BasicHttpBinding();

            Type implementation = typeof(RemoteControlService);

            string baseURL = "http://localhost:8000/RemoteControlService";

            ServiceHost sh = new ServiceHost(implementation, new Uri[] { new Uri(baseURL) });
    
            sh.AddServiceEndpoint(messengerContract, binding, baseURL);
            sh.AddServiceEndpoint(processManagerContract, binding, baseURL);
            sh.AddServiceEndpoint(taskManagerContract, binding, baseURL);

            var sb = new ServiceMetadataBehavior();
            sb.HttpGetEnabled = true;
            sh.Description.Behaviors.Add(sb);
            sh.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

            sh.Open();
            Console.ReadLine();
            sh.Close();
        }
    }
}
